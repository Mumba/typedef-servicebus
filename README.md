# Mumba Service Bus type definition

Type definitions for the Service Bus family of packages by [mateodelnorte](https://github.com/mateodelnorte).

## Installation

```sh
$ npm install --save mumba-typedef-servicebus
$ typings install servicebus=npm:mumba-typedef-servicebus --save --global
```

## People

The original author of _Mumba TypeDef Service Bus_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

