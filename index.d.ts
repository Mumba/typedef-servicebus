declare module "servicebus" {
	import {EventEmitter} from "events";

	export interface BusOptions {
		/**
		 * The URL for the AMQP server.
		 */
		url: string;

		vhost?: string;

		/**
		 * Default: 'amq.topic'
		 */
		exchangeName?: string;

		exchangeOptions?: any;
	}

	export class Bus extends EventEmitter {
		correlate: any;
		messageDomain: any;
		logger: any;
		package: any;
		retry: any;
		initialized: boolean;

		constructor(options: BusOptions);

		use(middleware: any): this;

		listen(queueName: string, options: any, callback: Function): void;
		listen(queueName: string, callback: Function): void;

		unlisten(queueName: string, options?: any): any; // TODO returns a Queue
		destroyListener(queueName: string, options?: any): void;

		// setOptions(queueName, options)
		send(queueName: string, message: any, options?: any, cb?: Function): void;

		// TODO typedef the return type which is the unsubscribe function I think.
		subscribe(queueName: string, options: any, callback: Function): any;
		subscribe(queueName: string, callback: Function): any;

		publish(queueName: string, message: any, options: any): void;
		publish(queueName: string, message: any, options: any, callback: Function): void;
		publish(queueName: string, message: any, callback: Function): void;

		close(): void;
	}

	export function bus(options?: any, implOpts?: any): Bus;
}

declare module "servicebus-retry" {
	export interface BusRetryEvent {
		// handle is optional so Entity events can be implement or extend from this
		handle?: {
			ack(callback?: (err?: Error) => void): void,
			acknowledge(callback?: (err?: Error) => void): void,
			reject(callback?: (err?: Error) => void): void
		}
	}
}
