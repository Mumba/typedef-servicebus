# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.1.5] 10 Nov 2016
## [0.1.4] 10 Nov 2016
- Added additional signature for `Bus.publish`.

## [0.1.3] 26 Sep 2016
## [0.1.2] 26 Sep 2016
Added `BusOptions` for the constructor, and some pub/sub API.

## [0.1.1] 19 Aug 2016
## [0.1.0] 19 Aug 2016
- Initial release.
